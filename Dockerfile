FROM openjdk:8
EXPOSE 8090
ENV SPRING_PROFILE=h2
RUN apt-get update && apt-get -y install netcat && apt-get clean
COPY target/assignment-*.jar.original /usr/local/app2.jar
COPY target/assignment-*.jar /usr/local/app.jar/
RUN chmod +x /usr/local/app.jar
ENTRYPOINT sleep 20; java -jar -Dspring.profiles.active=${SPRING_PROFILE} /usr/local/app.jar
